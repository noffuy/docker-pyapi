FROM  centos:7
LABEL noffuy <3330072-noffuy@users.noreply.gitlab.com>
 
RUN yum install -y bzip2 bzip2-devel gcc gcc-c++ make openssl-devel readline-devel zlib-devel \
    && yum install -y https://centos7.iuscommunity.org/ius-release.rpm \
    && yum install -y python35u python35u-libs python35u-devel python35u-pip \
    && pip3.5 install  --no-cache-dir -U bottle requests uwsgi  jinja2 \
    && rm -rf /var/cache/yum/*
 
RUN mkdir -p /opt/log
ADD ./src/* /opt/app/
 
EXPOSE 3035
 
RUN localedef -f UTF-8 -i ja_JP ja_JP.UTF-8
ENV LANG ja_JP.UTF-8
ENV LC_CTYPE "ja_JP.UTF-8"
ENV LC_NUMERIC "ja_JP.UTF-8"
ENV LC_TIME "ja_JP.UTF-8"
ENV LC_COLLATE "ja_JP.UTF-8"
ENV LC_MONETARY "ja_JP.UTF-8"
ENV LC_MESSAGES "ja_JP.UTF-8"
ENV LC_PAPER "ja_JP.UTF-8"
ENV LC_NAME "ja_JP.UTF-8"
ENV LC_ADDRESS "ja_JP.UTF-8"
ENV LC_TELEPHONE "ja_JP.UTF-8"
ENV LC_MEASUREMENT "ja_JP.UTF-8"
ENV LC_IDENTIFICATION "ja_JP.UTF-8"
ENV LC_ALL ja_JP.UTF-8
 
WORKDIR /opt/app
CMD uwsgi --ini  >> /opt/log/console.log 2>&1
